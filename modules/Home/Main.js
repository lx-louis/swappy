import styles from './Main.module.scss'

export default function Main() {
    return (
      <div className={styles.main}>
        <h1>SWЛPPY</h1>
        <h2>Swap at one place with official contract. <br/> No need for 12 bookmarks anymore</h2>
      </div>
    )
  }
  