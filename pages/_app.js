import '../styles.scss'
import Head from 'next/head'

function MyApp({ Component, pageProps }) {
  
  return (
    <div>
       <Head>
        <title>Swappy - First only Swap dApp</title>
        <meta name="description" content="swap swap !" />
        <link href="https://fonts.googleapis.com/css2?family=Comfortaa&family=Poppins:wght@300&display=swap" rel="stylesheet" />
        <link rel="shortcut icon" href="./favicon.png"></link>
      </Head>
      <Component {...pageProps}/>
    </div>
  )
}

export default MyApp
