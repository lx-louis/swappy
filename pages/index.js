import styles from '../styles/index.module.scss'

import Footer from '../layout/Footer'
import Header from '../layout/Header'
import Main from '../modules/Home/Main'

export default function Home() {
  return (
    <div className={styles.app}>
      <div className={styles.container}>
        <Header/>
        <Main/>
        <Footer/>
      </div>
    </div>
  )
}
